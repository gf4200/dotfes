﻿#pragma strict
private var myTransform:Transform;

//Update（毎フレーム呼ばれるfunction）の直前に呼ばれる
function Start ()
{
     //Transformコンポーネントを取得
     myTransform = transform;
}

//毎フレーム呼ばれる
function Update ()
{
     //デバイスの傾きを取得
     //値が(0, 0, 0, 1)になる x,y,z,w
     //var gyro:Quaternion = Input.gyro.attitude;

     //回転の向きの調整 
     //gyro.x *= -1.0; //x = x * -1.0
     //gyro.x = gyro.x * -1.5;
     //gyro.y *= -1.0;
     //gyro.y = gyro.y * -1.5;
     
     //自分の傾きとして適用
     //myTransform.localRotation = gyro;




var gyro:Quaternion = Input.gyro.attitude;
var rot = Quaternion(-gyro.x, -gyro.z, -gyro.y, gyro.w) * Quaternion.Euler(90f, 0f, 0f);

myTransform.localRotation = rot;

}

function OnGUI () {
     GUILayout.TextArea(Input.gyro.attitude.ToString());
}