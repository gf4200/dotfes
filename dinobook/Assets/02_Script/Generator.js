#pragma strict

var ShootingStar:GameObject;

function Start () {
	// ballPrefabを生成する,  第2引数　 : 位置,  第3引数　: 角度
	Instantiate(ShootingStar, transform.position, transform.rotation);
}

function Update () {
	// もしマウスが押されたら
	//if( Input.GetButtonDown("Fire1")) {
	// もし画面がタップされたら
	if(Input.GetTouch(0).phase == TouchPhase.Began){
		
		//ランダムな位置の計算
		 var xPos:Vector3 = Vector3(1, 0 ,0) * Random.Range(-5, 5);
		 //ボール生成の位置
		 var ballPos:Vector3 = transform.position + xPos;
		
		Instantiate(ShootingStar, ballPos, transform.rotation);
	}
}