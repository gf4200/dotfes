﻿#pragma strict

static var selectedGameObject : GameObject;

function Update () {

  var ray:Ray;
  var hit:RaycastHit;

  if(Input.GetMouseButtonDown(0)){
    ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    hit = new RaycastHit();

    if (Physics.Raycast(ray, hit, 100)) {
       selectedGameObject = hit.collider.gameObject;
       Debug.Log("name=" + selectedGameObject);
       if (selectedGameObject.name == "dino01") {
         Application.LoadLevel("book02");
       }
    } else {
      selectedGameObject = null;
    }
  }
}